const mongoose = require ('mongoose');

const dbConnect = async () => {
    try {
        await mongoose.connect ('mongodb://localhost:27017/basic_server', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log ('Connected to Mongodb server!');
    }
    catch (err) {
        console.log ('Mongodb connection error!');
    }
};

module.exports = dbConnect;