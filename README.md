# node_sample

A basic Mongo REST server.

Run:

```
npm run start

or

node index.js
```

Info:

* mongodb connection url (in db/mongoConnection.js) - mongodb://localhost:27017/basic_server

* Server Port (in index.json) - 5000

* POST JSON format - 

```
{
    "firstName" : "fname",
    "lastName" : "lname"
}
```

| Methods     | Endpoints        |
|:-------------:|:-------------:|
| GET  | /       |
| GET  | /list   |
| POST | /insert |


