const express = require ('express');

const mongoConnection = require ('./db/mongoConnection');
const User = require ('./models/user');

const app = express ();
const serverPort = 5000

// Middle wares
app.use (express.json ())

app.get ('/', async (req, res) => {
    res.write ('/       : manual\n');
    res.write ('/list   : list all objects in mongodb collection\n');
    res.write ('/insert : add an object to mongodb collection\n');
    res.end ();
});

app.get ('/list', async (req, res) => {
    try {
        // Projection to exclude _id and __v
        const allUsers = await User.find ({}).select('-_id -__v');
        res.send (allUsers);
    }
    catch (err) {
        res.status (500).send (err);
    }
});

app.post ('/insert', async (req, res) => {
    try {
        const newUser = new User (req.body);
        await newUser.save ();
        console.log ('Insert successfull!')
        res.status (201).send ('Object added to DB!');
    }
    catch (err) {
        res.status (500).send (err);
    }
});

app.listen (serverPort, async (req, res) => {
    try {
        await mongoConnection ();
        console.log ('Server running on port %i', serverPort);
    }
    catch (err) {
        console.log ('Server start error - %s', err);
    }
});

